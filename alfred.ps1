﻿#
# Author: DJ Kemmet, djkemmet@sftp.com
# Pupose: a centralized automation utility connected to SQL
# Requirements: dbatools, exhcange online shell
#

param (
    [string]$action,
    [string]$GeneralParameter1,
    [string]$GeneralParameter2,
    [string]$GeneralParameter3,
    [string]$GeneralParameter4
)
Import-Module ExchangeOnlineShell


# First Get the a credential object to use throughout the duration of the script execution session.
$Credential = Get-Credential

#Next, create One SQL Server Instance for the duration of the script execution session to update the database as needed.
function getSQLInstance()
{
    $username = $env:USERDOMAIN +"\" + $env:USERNAME
    $SqlInstance =  Connect-DbaInstance -SqlInstance LASSQLAUTO01.sixflags.local -Database alfred -SqlCredential $Credential -NetworkProtocol TcpIp -ClientName "Alfred Utility"
    return $SqlInstance
}

$SessionSQLServerInstance = getSQLInstance
# Creates the tables necessary for this utility to function
function initTables($sqlInstance)
{
    $SQLInstance = $sqlInstance
    
    # Create the SQL Tables necessary to keep track of all the email forwards we've requested.
    $initForwardLoggingTable = "
    
        use Alfred;
        BEGIN TRANSACTION
        SET QUOTED_IDENTIFIER ON
        SET ARITHABORT ON
        SET NUMERIC_ROUNDABORT OFF
        SET CONCAT_NULL_YIELDS_NULL ON
        SET ANSI_NULLS ON
        SET ANSI_PADDING ON
        SET ANSI_WARNINGS ON
        COMMIT
        BEGIN TRANSACTION
        GO
        CREATE TABLE dbo.emailForwards
	        (
	        fulfiller nchar(50) NULL,
	        fromEmail nchar(50) NULL,
	        toEmail nchar(50) NULL,
	        ticketNumber int NOT NULL,
	        datefulfilled nchar(50) NULL,
	        expDate nchar(50) NULL
	        )  ON [PRIMARY]
        GO
        ALTER TABLE dbo.emailForwards ADD CONSTRAINT
	        PK_Table_1 PRIMARY KEY CLUSTERED 
	        (
	        ticketNumber
	        ) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

        GO
        ALTER TABLE dbo.emailForwards SET (LOCK_ESCALATION = TABLE)
        GO
        COMMIT"

    Invoke-Sqlcmd -Query $initForwardLoggingTable -ServerInstance $SessionSQLServerInstance

}

# Install everything on the computer required for this script to work.
function installdeps
{
    # Install the modules necessary for SQL functionality...
    Write-Host "Installing Sql Server PowerShell Modules..."
    Set-PSRepository -Name PSGallery -InstallationPolicy Trusted
    Install-Module -Name dbaTools –Scope CurrentUser -Confirm:$false -AllowClobber
    Write-Host "SQL Server PowerShell modules are installed!"

    # Install the modules necessary for Exchange Functionality...
    Write-Host "Installing Exchange PowerShell Modules..."
    Install-Module ExchangeOnlineShell

}

#To be honest, I'm not sure why or if I need this in Mr.Six
function Connect-ExchangeOnline{
    $UPN = "$env:USERNAME@sftp.com"
    $UserCredential = Get-Credential -UserName $UPN -Message "Enter your password"
    Connect-EOShell -Credential $SessionCredentials
    $UserCredential = $null

  }

# Small function to validate email addresses submitted as parameters.
function validateEmailAddress($emailAddress)
{
    # Break email apart so that it can be validated.
    $emailComponents = $emailAddress.split("@")
    
    # Create variables from the components of the email address
    $emailDomain = $emailComponents[1]

    # Check the email domain to make sure it's a sixflags email.
    if ($emailDomain -notlike "sftp.com")
    {
        write-Host "You are attempting to configure an email forward outside the organization. this action cannot be completed."
        return $false
    }

    return $true
}

#Update mrsix.dbo.EmailForwards table.
function updateForwardLog($columnData)
{

    # Unpack our array into usable variables that can be substituted into a sql query
    $fulfiller = $columnData[0]
    $fromEmail = $columnData[1]
    $toEmail = $columnData[2]
    $ticketNumber = $columnData[3]
    $datefulfilled = ((Get-Date).ToString()).split(" ")[0]
    $expDate = ((Get-Date).adddays(90).ToString()).split(" ")[0]

    # Substitute our variables into the SQL Query
    $emailLogQuery = "use Alfred; Insert into [emailForwards] (fulfiller, fromEmail, toEmail, ticketNumber, datefulfilled, expDate) Values (" +"'" + $fulfiller + "', '" + $fromEmail + "', '" + $toEmail + "', '" + $ticketNumber + "', '" + $datefulfilled + "', " + "'" + $expDate + "');"
    Invoke-Sqlcmd -Query $emailLogQuery -ServerInstance $SessionSQLServerInstance
}

# Configures email forwards and updates the database with an expiration
function configureEmailForward($requestData)
{
   
   $FromEmail = $requestData[0]
   $ToEmail = $requestData[1]
   $ticketNumber = $requestData[2]
 
   if ($ticketNumber -eq ""){
       Write-host "A ticket number is required to fulfill this request please enter it below:"
       $ticketNumber = Read-Host 
   }
   
    # Check the FROM email to make sure it is valid. If it is valid...
    if (validateEmailAddress($FromEmail) -eq $true)
    {

        Write-Host "Email Address 1 is valid"
        
        #Check the TO email to make sure it is valid. If it is valid...
        if (validateEmailAddress($ToEmail) -eq $true)
        {
            #
            # Both addresses are OK
            # CODE TO SET RED AND FORWARD MAILBOX HERE.
            #
            #

            Connect-ExchangeOnline -credential 

            # Change the configuration of the mailbox and let the user know. 
            Write-Host "Setting Mailbox to shared..."
            Set-Mailbox $FromEmail -Type Shared

            # Set the forward to the new user and let the user know.
            Write-Host "Configuring the Email forward to recieving user..."
            Set-Mailbox $FromEmail -DeliverToMailboxAndForward $true -ForwardingAddress $ToEmail
            

            # Get information about this request together for the database.
            $columnData = @( $env:USERNAME, $FromEmail, $ToEmail, $ticketNumber)

            updateForwardLog($columnData)
        }

        # If the TO email address is bad, let the user know and exit.
        elseif (validateEmailAddress($ToEmail) -eq $false)
        {

            # Let the user know there was a problem with the email address they supplied and exit the application.
            Write-host "your email address" + $GeneralParameter2 + "did not pass validation, please try your request again"
            exit
        }
    }

    # If the FROM email address is bad, let the user know and exit.
    elseif (validateEmailAddress($FromEmail) -eq $false)
    {
        # Let the user know there was a problem with the email address they supplied and exit the application.
        Write-host "your email address" + $GeneralParameter1 + "did not pass validation, please try your request again"
        exit
    }
}

# The user wants to configure an email forward
switch ($action) {


    "configureForward" 
        {
            #                 From Email Address  To Email Address    Ticket Number
            $requestData = @($GeneralParameter1, $GeneralParameter2, $GeneralParameter3)
            configureEmailForward($requestData)
        }

    "installdeps" 
        {
            installdeps
        }

    "initutil" 
        {
            initTables(getSQLInstance)
        }
    }


