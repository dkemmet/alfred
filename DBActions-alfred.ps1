# Connect to LASSQLAUTO01
# AUTHOR: DJ Kemmet, djkemmet@sftp.com
# Purpose: A Once-daily Task to check for email forwards that are expriing and expire them, then send an email confirmation. 

function getSQLInstance()
{
    Import-Module DbaTools
    $username = $env:USERDOMAIN +"\" + $env:USERNAME
    $SqlInstance =  Connect-DbaInstance -SqlInstance LASSQLAUTO01.sixflags.local -Database Alfred -SqlCredential $username -NetworkProtocol TcpIp -ClientName "Alfred Utility"
    return $SqlInstance
}

Function SendEmailStatus($ListOfExpirations)
{
    $MessageBodyTop = @"
    <!Doctype html5>
    <html>
        <body>
            <div class="row">
                <div class="col-md-6" style="align:center;">
                    <h1 style="text-align: center;">Good Morning...</h1>
                    <p style="text-align: center;">I took the liberty of taking the following actions for you:</p>
                </div>
                <div class="row">
"@ #Terminates where the body opened

        # For each noritifcation that was passed to this function, add a list item to the HTML Message body, end with a new line. 
        for ($counter=0; $counter -lt $ListOfExpirations.Length; $counter++){
            $MessageBodyTop += "<li>" + $ListOfExpirations[$counter] + "</li>" +"`n"
        }
    
    # Close the HTML Message Body.    
    $MessageBodyBottom = @"
    </div>
    </div>
</body>
</html>
"@

    #Make one big HTML message body.
    $MessageBody = $MessageBodyTop + $MessageBodyBottom
    
    # Set up our particulars
    $SMTPServer = "smtp.sftp.com"
    $FromAddress = "djkemmet@sftp.com"
    $Recipients = "djkemmet@sftp.com"
    $MessageSubject = "Alfred's Morning Report for" + (Get-date -Format " dddd MM/dd/yyyy")

    $SMTPMessage = New-Object System.Net.Mail.MailMessage $FromAddress, $Recipients, $MessageSubject, $MessageBody
    $SMTPMessage.IsBodyHTML = $true
    #Send the message via the local SMTP Server
    $SMTPClient = New-Object System.Net.Mail.SMTPClient $SMTPServer
    $SMTPClient.Send($SMTPMessage)
    $SMTPMessage.Dispose()
    Remove-Variable SMTPClient
    Remove-Variable SMTPMessage
}


function checkExpirations{
    # Get a SQL Session and get
    $SQLInstance = getSQLInstance
    $ListOfExpirations = @()

    # Imports necessary for the script to run.
    Import-Module ExchangeOnlineShell 
    Connect-ExchangeOnline -credential 

    # Get all Forwards that expire today...
    $TodaysDate = ((Get-Date).ToString()).split(" ")[0]
    $query = "Select * From dbo.emailForwards Where datefulfilled in ('<DATE>');".replace("<DATE>", $TodaysDate)
    Write-Host $query

    # Run Our Query and return a powershell object containing our rows. 
    $resultSet = Invoke-DbaQuery -SqlInstance $SQLInstance -Query $query -As PSObject

    # For each result returned from the database table...
    $resultSet | ForEach-Object -Process {

        #Stringamatize email address in PS object....
        $ForwardFromEmail = ($_.fromEmail).replace(" ", "")
        $ForwardToEmail = ($_.toEmail).replace(" ", "")

        # Use afore-mentioned stringamafication to execute yonder pawershell scrimps...
        Write-host Removing forward from account $ForwardToEmail...
        Set-Mailbox $ForwardFromEmail -forwardingaddress $null -DeliverToMailboxAndForward $false

        # Use the stringimification to remove all permissions from the recivivng exchange account
        Write-host Removing Privilges from account $ForwardToEmail...
        Remove-MailboxPermission $ForwardToEmail -user $ForwardFromEmail -AccessRights fullaccess -InheritanceType none 

        # Set the forwarded mailbox back to a regular mailbox type
        Write-Host "Converting " $ForwardFromEmail "account back to standard email account."
        Set-Mailbox $ForwardFromEmail -Type Regular

        # Now that we've made our changes let's remove the row from the db.
        $query = "DELETE FROM dbo.emailForwards where TicketNumber in ('<TICKETNUMBER>') ".replace("<TICKETNUMBER>",$_.TicketNumber)
        Invoke-DbaQuery -SqlInstance $SQLInstance -Query $query -As PSObject

        # Create a notification and add it to a list for addition to the notification email. 
        $Notification =  "<b>EMAIL FORWARD:</b> The forward for ticket " + "<a href='https://sixflags.samanage.com/incidents/<ticket>'>".replace("<ticket>", $_.TicketNumber) + $_.TicketNumber + "</a>" + " has been removed and Privilges revoked."
        $ListOfExpirations += $notification #Send a list of email forwards that have been removed. 

        } 

    # When all emails have been processed, send a status email.
    SendEmailStatus($ListOfExpirations)
    }

    #
    #
    # SCRIPT BODY
    # Execute all the actions here. 
    #
    3
checkExpirations





